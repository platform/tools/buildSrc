/*
 * Copyright (C) 2021 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.android.tools.internal.bazel;

import org.gradle.api.file.Directory;
import org.gradle.api.file.DirectoryProperty;
import org.gradle.api.logging.Logging;
import org.gradle.api.provider.Property;
import org.gradle.api.provider.Provider;
import org.gradle.api.services.BuildService;
import org.gradle.api.services.BuildServiceParameters;
import org.gradle.process.ExecOperations;

import javax.annotation.concurrent.GuardedBy;
import javax.annotation.concurrent.ThreadSafe;
import javax.inject.Inject;
import java.io.File;
import java.io.IOException;
import java.io.UncheckedIOException;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@ThreadSafe
public abstract class BazelPrebuiltsBuildService implements BuildService<BazelPrebuiltsBuildService.Params>, AutoCloseable {

    public BazelPrebuiltsBuildService() {
        Logging.getLogger(BazelPrebuiltsBuildService.class).lifecycle("Using hybrid build");
    }

    public interface Params extends BuildServiceParameters {
        DirectoryProperty getRootDir();
        Property<String> getOsName();
        Property<Boolean> getUseReleaseVersion();
    }

    @Inject
    public abstract ExecOperations getExecOperations();

    @GuardedBy("this")
    private boolean success = false;
    @GuardedBy("this")
    private UncheckedIOException failure;

    public synchronized void ensurePrebuiltsAreBuilt() {
        if (failure != null) {
            throw failure;
        }
        if (success) {
            return;
        }

        try {
            invokeBazel();
        } catch (IOException e) {
            failure = new UncheckedIOException(e);
            throw failure;
        }

        success = true;
    }

    public Provider<Directory> getMavenRepoLocation() {
        return getParameters()
                .getRootDir()
                .dir("../out/repo");
    }

    private void invokeBazel() throws IOException {
        File bazel = getParameters()
                .getRootDir()
                .file(getBazelExe())
                .get()
                .getAsFile();
        List<String> args = new ArrayList<>();
        args.add("run");
        if (!getParameters().getRootDir().dir("vendor").get().getAsFile().isDirectory()) {
            Logging.getLogger(BazelPrebuiltsBuildService.class).lifecycle("Running in AOSP mode");
            args.add("--config=without_vendor");
        }
        if (getParameters().getUseReleaseVersion().get()) {
            args.add("--config=release");
        }
        args.add("//tools/base:agp_artifacts_dir");
        args.add("--");
        args.add(getMavenRepoLocation().get().getAsFile().getAbsolutePath());

        Logging.getLogger(BazelPrebuiltsBuildService.class)
                .lifecycle(
                        "Running bazel build:\n  " + bazel.getAbsolutePath() + " \\\n" + args.stream().collect(Collectors.joining(" \\\n     ")));
        getExecOperations()
                .exec(
                        spec -> {
                            spec.setWorkingDir(getParameters().getRootDir());
                            spec.executable(bazel);
                            spec.args(args);
                        });
    }

    private String getBazelExe() {
        if (getParameters().getOsName().get().startsWith("Windows")) {
            return "base/bazel/bazel.cmd";
        } else {
            return "base/bazel/bazel";
        }
    }

    @Override
    public synchronized void close() throws Exception {
        success = false;
        failure = null;
    }
}
